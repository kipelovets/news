News API
========

Данное API предоставляет доступ к данным системе аггрегации новостей по тематике Starcraft2

Взаимодействие с API
====================

Для обращения к API необходимо сделать GET запрос по протоколу HTTP на следующий URL:
`http://<hostname>/<method>[?<parameters>]`

где:

* hostname - IP-адрес сервера API (обратитесь к kipelovets@mail.ru для получения адреса);
* method - один из методов API (help, tags, news; подробнее см. ниже);
* parameters - опциональный список параметров, см. ниже

По умолчанию возвращается ответ в формате HTML, например:


```
	$ curl http://hostname/tags
	<h1>Tags</h1><ul><li><a href='/news?tags=BattleNet'>BattleNet</a></li>...</ul>
```


Для получения ответа в формате JSON необходимо передать HTTP заголовок `Accept: application/json`, например:

```
	$ curl -H 'Accept: application/json' http://hostname/tags
	{"response":["BattleNet",...]}
```

Если запрос выполнен успешно, возвращается HTTP код 200. В противном случае возвращается код 400 (если запрос составлен
неправильно) или 500 (если на сервере при выполнении запроса произошла ошибка), например:

```
	$ curl -D - -H 'Accept: application/json' http://hostname/
	HTTP/1.1 400 Bad Request
	Content-Type: application/json
	
	{"error":{"message":"Method not found, try \/help"}}
```

Методы API
==========

/tags
-----

Возвращает список тегов в системе

/news
-----

Возвращает список новостей, упорядоченных по дате публикации. Опциональные параметры:

* count - максимальное количество возвращаемых новостей (не более 100);
* dateFrom - минимальная дата публикации новости в формате, распознаваемом PostgreSQL 
  (`http://www.postgresql.org/docs/9.1/static/datatype-datetime.html#DATATYPE-DATETIME-DATE-TABLE`).
  Системный часовой пояс — московское время (UTC+3);
* dateTo - максимальная дата публикации новости, аналогично dateFrom;
* tags - список тегов для фильтрации новостей через запятую;
* maxWords - максимальное число слов возвращаемого текста новостей (по умолчанию возвращается полный текст);
 
Пример:

```
	$ curl -H 'Accept: application/json' 'http://hostname/news?tags=BattleNet&count=1&dateFrom=2015-03-01&dateTo=2015-05-01%2013:00&maxWords=3' 
	{"response":[{"id":50,"title":"К оружию: тестирование баланса (26 марта)","published":"2015-03-27 23:08:12",
	"sourceurl":"http:\/\/eu.battle.net\/sc2\/ru\/blog\/18483058\/",
	"text":"Мы обновили карту","tags":"BattleNet"}]}
```

Каждая новость содержит следующие поля:

* id - внутренний идентификатор
* title - заголовок новости
* published - дата/время публикации 
* sourceurl - ссылка на источник 
* text - тест новости
* tags - список тегов новости через запятую

/news/<id>
----------

Возвращает одну новость по внутреннему идентификатору id, например:

```
	$ curl -H 'Accept: application/json' 'http://hostname/news/215'
	{"response":{"id":215,"title":"StarCraft 2 исполняется 4 года","published":"2014-07-26 22:31:34",
	"sourceurl":"http:\/\/www.starcraft.7x.ru\/?p=ncomments&nid=3545&action=lo","text":"А тем временем сегодня...",
	"tags":"Starcraft7x"}}
```

/help
-----

Возвращает справку по API