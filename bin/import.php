<?php
/**
 * kipelovets <kipelovets@mail.ru>
 */

use News\Importer;

require_once(__DIR__ . '/../config/app.php');

/** @var Importer $importer */
$importer = $app['importer'];
$importer->run();