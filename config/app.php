<?php
/**
 * kipelovets <kipelovets@mail.ru>
 */

use Dflydev\Provider\DoctrineOrm\DoctrineOrmServiceProvider;
use Silex\Provider\DoctrineServiceProvider;

require_once(__DIR__ . '/../vendor/autoload.php');

date_default_timezone_set('Europe/Moscow');

$app = new Silex\Application();

$app['config'] = require_once(__DIR__ . '/config.php');

$app->register(new DoctrineServiceProvider, [
    "db.options" => [
        "driver" => "pdo_pgsql",
        "host" => $app['config']['db']['host'],
        "port" => $app['config']['db']['port'],
        "dbname" => $app['config']['db']['dbname'],
        "user" => $app['config']['db']['user'],
        "password" => $app['config']['db']['password'],
    ],
]);

$app->register(new DoctrineOrmServiceProvider, [
    "orm.proxies_dir" => __DIR__ . "/../src/Entity/Proxy",
    "orm.em.options" => [
        "mappings" => [
            [
                "type" => "annotation",
                "namespace" => "News\\Entity",
                "path" => __DIR__."/../src/Entity",
            ],
        ],
    ],
]);

$app->register(new Silex\Provider\MonologServiceProvider(), array(
    'monolog.logfile' => 'php://stdout',
));

$app['importer'] = function () use ($app) {
    $importer = new \News\Importer($app['orm.em'], [
        new \News\Parser\VkGroup($app['config']['tags']),
        new \News\Parser\Starcraft7x($app['config']['tags']),
        new \News\Parser\BattleNet($app['config']['tags']),
    ]);
    $importer->setLogger($app['monolog']);
    return $importer;
};

