<?php
/**
 * kipelovets <kipelovets@mail.ru>
 */

use Doctrine\ORM\EntityManager;

require_once(__DIR__ . '/app.php');

/** @var EntityManager $em */
$em = $app['orm.em'];
$helpers = new Symfony\Component\Console\Helper\HelperSet([
    'db' => new \Doctrine\DBAL\Tools\Console\Helper\ConnectionHelper($em->getConnection()),
    'em' => new \Doctrine\ORM\Tools\Console\Helper\EntityManagerHelper($em)
]);