<?php

return [
    'db' => [
        'host' => '188.166.24.226',
        'port' => '5432',
        'dbname' => 'news',
        'user' => 'postgres',
        'password' => 'postgres',
    ],
    'tags' => [
        'BlizzCon',
        'Heart of the Swarm',
        'Legacy of the Void',
    ],
    'help' => file_get_contents(__DIR__ . '/../README.md'),
];