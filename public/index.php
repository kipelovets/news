<?php
/**
 * kipelovets <kipelovets@mail.ru>
 */

require_once(__DIR__ . '/../config/app.php');

$app->register(new \News\View\ViewProvider());
$app->mount('/', new \News\Controller\ControllerProvider());

$app->run();