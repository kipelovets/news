<?php
/**
 * kipelovets <kipelovets@mail.ru>
 */

namespace News\Controller;

use Doctrine\DBAL\Connection;
use Symfony\Component\HttpFoundation\Request;

class Controller
{
    /** @var Connection */
    private $db;

    /** @var string */
    private $help;

    function __construct(Connection $db, $help)
    {
        $this->db = $db;
        $this->help = $help;
    }

    /**
     * @return array
     * @throws \Doctrine\DBAL\DBALException
     */
    public function actionTags()
    {
        $tags = $this->db
            ->query("SELECT tag FROM tag")
            ->fetchAll(\PDO::FETCH_COLUMN, 0);
        return $tags;
    }

    /**
     * @param Request $request
     * @return array
     * @throws \Doctrine\DBAL\DBALException
     */
    public function actionNews(Request $request)
    {
        $count = (int)$request->get('count', 100);
        $count = min(100, $count);
        $dateFrom = $request->get('dateFrom');
        $dateTo = $request->get('dateTo');
        $tags = $request->get('tags', '');
        $maxWords = (int)$request->get('maxWords', -1);

        list($query, $params, $paramTypes) = $this->buildQuery($dateFrom, $dateTo, $tags, $count);
        $stmt = $this->db->executeQuery($query, $params, $paramTypes);
        $result = $stmt->fetchAll();

        if ($maxWords != -1) {
            $this->limitWords($result, $maxWords);
        }

        return $result;
    }

    public function actionArticle($id)
    {
        $query = <<<ENDS
SELECT
    a.id,
    a.title,
    a.published,
    a.sourceUrl,
    a.text,
    array_to_string(array(SELECT tag_id FROM articles_tags AS at WHERE at.article_id = a.id), ',') AS tags
FROM article AS a WHERE id = ?
ENDS;

        $article = $this->db
            ->executeQuery($query, [$id])
            ->fetch();
        return $article;
    }

    public function actionHelp()
    {
        return $this->help;
    }

    private function buildQuery($dateFrom, $dateTo, $tags, $count)
    {
        $textExpr = 'a.text';
        $params = [];
        $paramTypes = [];
        $where = [];

        if ($dateFrom) {
            $params[] = $dateFrom;
            $paramTypes[] = \PDO::PARAM_STR;
            $where[] = "published >= ?";
        }

        if ($dateTo) {
            $params[] = $dateTo;
            $paramTypes[] = \PDO::PARAM_STR;
            $where[] = "published <= ?";
        }

        if ($tags) {
            $where[] = "EXISTS(SELECT * FROM articles_tags AS at WHERE at.article_id = a.id AND at.tag_id IN (?)) " ;
            $params[] = explode(',', $tags);
            $paramTypes[] = Connection::PARAM_STR_ARRAY;
        }
        $params[] = $count;
        $paramTypes[] = \PDO::PARAM_INT;

        $whereString = (count($where) > 0 ? "WHERE " : "") . implode(" AND ", $where);
        $query = <<<SQL
SELECT
    a.id,
    a.title,
    a.published,
    a.sourceUrl,
    $textExpr,
    array_to_string(array(SELECT tag_id FROM articles_tags AS at WHERE at.article_id = a.id), ',') AS tags
  FROM article AS a
    $whereString
  ORDER BY published
  LIMIT ?
SQL;
        return [$query, $params, $paramTypes];
    }

    /**
     * @param array $result
     * @param $maxWords
     * @return mixed
     */
    private function limitWords(array &$result, $maxWords)
    {
        array_walk($result, function (&$row) use ($maxWords) {
            $text = trim(preg_replace("/^(\\W*(\\w+\\W*){" . $maxWords . "}).*$/us", '$1', $row['text']));
            $row['text'] = $text;
        });
    }
}