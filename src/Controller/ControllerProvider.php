<?php
/**
 * kipelovets <kipelovets@mail.ru>
 */

namespace News\Controller;

use Silex\Api\ControllerProviderInterface;
use Silex\Application;
use Silex\ControllerCollection;
use Symfony\Component\HttpKernel\Exception\HttpException;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

class ControllerProvider implements ControllerProviderInterface
{
    /**
     * Returns routes to connect to the given application.
     *
     * @param Application $app An Application instance
     *
     * @return ControllerCollection A ControllerCollection instance
     */
    public function connect(Application $app)
    {
        $this->bindErrorHandler($app);

        $controller = $this->createController($app);
        $controllersFactory = $app['controllers_factory'];
        $this->bindRegularRoutes($controllersFactory, $controller);
        $this->bindFallbackRoute($controllersFactory);
        return $controllersFactory;
    }

    /**
     * @param Application $app
     * @return mixed
     */
    private function createController(Application $app)
    {
        return new Controller($app['db'], $app['config']['help']);
    }

    /**
     * @param Application $app
     */
    private function bindErrorHandler(Application $app)
    {
        $app->error(function (\Exception $e) {
            switch (true) {
                case $e instanceof NotFoundHttpException:
                    return new ErrorResponse("Method not found, try /help", 400);

                case $e instanceof HttpException:
                    return new ErrorResponse("Http error: " . $e->getMessage(), 400);

                default:
                    return new ErrorResponse(get_class($e) . ':' . $e->getMessage(), 500);
            }
        });
    }

    /**
     * @param ControllerCollection $controllers
     */
    private function bindFallbackRoute(ControllerCollection $controllers)
    {
        $handler = function () {
            throw new NotFoundHttpException();
        };

        $controllers->match('/', $handler);
        $controllers->match('{url}', $handler);
    }

    /**
     * @param ControllerCollection $controllers
     * @param Controller $controller
     */
    private function bindRegularRoutes(ControllerCollection $controllers, Controller $controller)
    {
        $routes = [
            '/tags' => 'tags',
            '/news' => 'news',
            '/news/{id}' => 'article',
            '/help' => 'help',
        ];

        foreach ($routes as $route => $routeName) {
            $methodName = "action{$routeName}";
            $controllers->get($route, [$controller, $methodName])
                ->bind($routeName);
        }
    }
}