<?php
/**
 * kipelovets <kipelovets@mail.ru>
 */

namespace News\Controller;

class ErrorResponse
{
    public $content;
    public $code;

    function __construct($content, $code)
    {
        $this->content = $content;
        $this->code = $code;
    }
}