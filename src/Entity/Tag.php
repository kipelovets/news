<?php
/**
 * kipelovets <kipelovets@mail.ru>
 */

namespace News\Entity;

use Doctrine\ORM\Mapping\Column;
use Doctrine\ORM\Mapping\Entity;
use Doctrine\ORM\Mapping\Id;

/**
 * @Entity()
 */
class Tag 
{
    /**
     * @Id
     * @Column(length=1024)
     */
    private $tag;

    /**
     * @return mixed
     */
    public function getTag()
    {
        return $this->tag;
    }

    /**
     * @param mixed $tag
     */
    public function setTag($tag)
    {
        $this->tag = $tag;
    }
}