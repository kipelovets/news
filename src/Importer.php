<?php
/**
 * kipelovets <kipelovets@mail.ru>
 */

namespace News;

use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\AbstractQuery;
use Doctrine\ORM\EntityManager;
use News\Entity\Tag;
use News\Parser\ParserInterface;
use Psr\Log\LoggerAwareInterface;
use Psr\Log\LoggerAwareTrait;

class Importer implements LoggerAwareInterface
{
    use LoggerAwareTrait;

    /** @var EntityManager */
    private $em;
    /** @var Parser\ParserInterface[] */
    private $parsers;

    /**
     * @param EntityManager $em
     * @param ParserInterface[] $parsers
     */
    public function __construct(EntityManager $em, $parsers)
    {
        $this->em = $em;
        $this->parsers = $parsers;
    }

    public function run()
    {
        foreach ($this->parsers as $parser) {
            $this->logger->info("Executing parser.", ['parser' => get_class($parser)]);
            foreach ($parser->getNewArticleUrls() as $articleUrl) {
                $query = $this->em->createQuery("SELECT COUNT(a.id) FROM News\\Entity\\Article a WHERE a.sourceUrl = :url");
                $count = $query->execute(['url' => $articleUrl], AbstractQuery::HYDRATE_SINGLE_SCALAR);
                if ($count == 0) {
                    $this->logger->info("Processing article.", ['url' => $articleUrl]);
                    $article = $parser->loadArticle($articleUrl);
                    $this->replaceTagsWithEntities($article->getTags());
                    $this->em->persist($article);
                } else {
                    $this->logger->info("Skipping.", ['url' => $articleUrl]);
                }
            }
        }
        $this->em->flush();
        $this->logger->info("Done.");
    }

    /**
     * @param Collection $collection
     * @throws \Doctrine\ORM\ORMException
     * @throws \Doctrine\ORM\OptimisticLockException
     * @throws \Doctrine\ORM\TransactionRequiredException
     */
    private function replaceTagsWithEntities(Collection $collection)
    {
        foreach ($collection as $key => $tag) {
            if (!is_object($tag)) {
                $tagEntity = $this->em->find('News\\Entity\\Tag', $tag);
                if (!$tagEntity) {
                    $tagEntity = new Tag();
                    $tagEntity->setTag($tag);
                    $this->em->persist($tagEntity);
                }
                $collection->set($key, $tagEntity);
            }
        }
    }
}