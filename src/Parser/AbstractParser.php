<?php
/**
 * kipelovets <kipelovets@mail.ru>
 */

namespace News\Parser;

use Goutte\Client;
use News\Entity\Article;
use Symfony\Component\DomCrawler\Crawler;

abstract class AbstractParser implements ParserInterface
{
    use TagsExtractor;

    /** @var array */
    protected $tags;

    /** @var Client */
    protected $client;

    /**
     * @param array $tags
     */
    public function __construct(array $tags)
    {
        $this->tags = $tags;
        $this->client = new Client();
    }

    /**
     * @return \Traversable
     */
    abstract public function getNewArticleUrls();

    /**
     * @param string $url
     * @return Article
     */
    public function loadArticle($url)
    {
        $crawler = $this->client->request('GET', $url);

        $article = new Article();

        $article->setSourceUrl($url);
        $article->setTitle($this->filterTitle($crawler));
        $article->setPublished($this->filterDate($crawler));

        $text = $this->filterText($crawler);
        $article->setText($text);

        foreach ($this->extractTags($text, $this->tags) as $tag) {
            $article->getTags()->add($tag);
        }
        return $article;
    }

    /**
     * @param $crawler
     * @return mixed
     */
    abstract protected function filterTitle(Crawler $crawler);

    /**
     * @param $crawler
     * @return string
     */
    abstract protected function filterText(Crawler $crawler);

    /**
     * @param $crawler
     * @return \DateTime
     */
    abstract protected function filterDate(Crawler $crawler);
}