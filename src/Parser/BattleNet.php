<?php
/**
 * kipelovets <kipelovets@mail.ru>
 */

namespace News\Parser;

use Symfony\Component\DomCrawler\Crawler;

class BattleNet extends AbstractParser implements ParserInterface
{
    const BASE_URL = 'http://eu.battle.net';
    const NEWS_PAGE = '/sc2/ru/';

    /**
     * @return \Traversable
     */
    public function getNewArticleUrls()
    {
        $crawler = $this->client->request('GET', self::BASE_URL . self::NEWS_PAGE);
        $nodes = $crawler->filter('.article-wrapper > a');
        foreach ($nodes as $node) {
            $url = $node->getAttribute('href');
            if (strpos($url, 'http:') !== 0) {
                $url = self::BASE_URL . $url;
            }
            yield $url;
        }
    }

    /**
     * @param $crawler
     * @return mixed
     */
    protected function filterTitle(Crawler $crawler)
    {
        $titleNode = $crawler->filter('[itemprop=headline]');
        if ($titleNode->count() == 0) {
            $titleNode = $crawler->filter('.section__title');
        }
        return $titleNode->text();
    }

    /**
     * @param $crawler
     * @return string
     */
    protected function filterText(Crawler $crawler)
    {
        $node = $crawler->filter('[itemprop=articleBody]');
        if ($node->count() == 0) {
            $node = $crawler->filter('.heroes-blog');
        }
        $node->filter('style')->each(function (Crawler $crawler) {
            foreach ($crawler as $node) {
                $node->parentNode->removeChild($node);
            }
        });
        return strip_tags(trim($node->text()));
    }

    /**
     * @param $crawler
     * @return \DateTime
     */
    protected function filterDate(Crawler $crawler)
    {
        $date = trim($crawler->filter('.publish-date')->text());
        return \DateTime::createFromFormat('d/m/Y', $date);
    }
}