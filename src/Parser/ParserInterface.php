<?php
/**
 * kipelovets <kipelovets@mail.ru>
 */
namespace News\Parser;

use News\Entity\Article;

interface ParserInterface
{
    /**
     * @return \Traversable
     */
    public function getNewArticleUrls();

    /**
     * @param string $url
     * @return Article
     */
    public function loadArticle($url);
}