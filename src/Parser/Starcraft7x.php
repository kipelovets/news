<?php
/**
 * kipelovets <kipelovets@mail.ru>
 */

namespace News\Parser;

use Symfony\Component\DomCrawler\Crawler;

class Starcraft7x extends AbstractParser implements ParserInterface
{
    const NEWS_PAGE = 'http://starcraft.7x.ru/rss/rss.php';

    /**
     * @return \Traversable
     */
    public function getNewArticleUrls()
    {
        $feed = $this->client->request('GET', self::NEWS_PAGE);
        foreach ($feed->filterXPath('//item/link') as $node) {
            /** @var \DOMElement $node */
            yield $node->textContent;
        }
    }

    /**
     * @param Crawler $page
     * @return \DateTime
     */
    protected function filterDate(Crawler $page)
    {
        $text = $page->filter('#new1drop')->text();
        preg_match_all("/@ [0-9-]+ [0-9:]+/", $text, $matches);
        $date = null;
        foreach ($matches[0] as $maybeDate) {
            try {
                $date = new \DateTime(substr($maybeDate, 2));
            } catch (\Exception $e) {
                // U no nothing, Jon Snow
            }
        }
        return $date;
    }

    /**
     * @param Crawler $page
     * @return string
     */
    protected function filterTitle(Crawler $page)
    {
        return $page->filter('h1')->text();
    }

    /**
     * @param $crawler
     * @return string
     */
    protected function filterText(Crawler $crawler)
    {
        return strip_tags(trim($crawler->filter('sape_index')->text()));
    }
}