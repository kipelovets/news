<?php
/**
 * kipelovets <kipelovets@mail.ru>
 */

namespace News\Parser;

trait TagsExtractor
{
    /**
     * @param $text
     * @param array $tags
     * @return \Generator
     */
    protected function extractTags($text, array $tags)
    {
        $class = get_class($this);
        yield substr($class, strrpos($class, '\\') + 1);
        foreach ($tags as $tag) {
            if (strpos($text, $tag) !== false) {
                yield $tag;
            }
        }
    }
}