<?php
/**
 * kipelovets <kipelovets@mail.ru>
 */

namespace News\Parser;

use News\Entity\Article;

class VkGroup implements ParserInterface
{
    use TagsExtractor;

    const FEED_URL = 'https://api.vk.com/method/wall.get?domain=starcraft';
    const ARTICLE_URL_PREFIX = 'https://api.vk.com/method/wall.getById?posts=';
    const ARTICLE_PREFIX = 'https://vk.com/wall';

    /** @var array */
    private $tags;
    /** @var array */
    private $cache;

    /**
     * @param array $tags
     */
    public function __construct(array $tags)
    {
        $this->tags = $tags;
        $this->cache = [];
    }

    /**
     * @return \Traversable
     */
    public function getNewArticleUrls()
    {
        $data = json_decode(file_get_contents(self::FEED_URL), true);
        foreach ($data['response'] as $row) {
            if (is_array($row)) {
                $url = self::ARTICLE_PREFIX . "{$row['from_id']}_{$row['id']}";
                $this->cache[$url] = $row;
                yield $url;
            }
        }
    }

    /**
     * @param string $url
     * @return Article
     */
    public function loadArticle($url)
    {
        $articleData = $this->cache[$url];
        $text = $articleData['text'];
        $article = new Article();
        $article->setSourceUrl($url);
        $article->setTitle(preg_replace('/^([^.!?]+).*$/', '$1', $text));
        $article->setText($text);
        $article->setPublished(new \DateTime('@' . $articleData['date']));
        foreach ($this->extractTags($text, $this->tags) as $tag) {
            $article->getTags()->add($tag);
        }
        return $article;
    }
}