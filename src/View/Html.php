<?php
/**
 * kipelovets <kipelovets@mail.ru>
 */

namespace News\View;

use News\Controller\ErrorResponse;
use Symfony\Component\HttpFoundation\Response;

class Html implements ViewInterface
{
    /**
     * @param $route
     * @param $data
     * @return Response
     * @throws ViewException
     */
    public function render($route, $data)
    {
        if ($data instanceof ErrorResponse) {
            return $this->renderError($data);
        }

        $method = "render$route";
        if (method_exists($this, $method)) {
            return $this->$method($data);
        }

        if ($data instanceof ViewException) {
            return new Response("Internal error", 500);
        }

        throw new ViewException("View not found for route");
    }

    /**
     * @param array $tags
     * @return Response
     */
    public function renderTags(array $tags)
    {
        $content = "<h1>Tags</h1><ul>";
        foreach ($tags as $tag) {
            $content .= "<li><a href='/news?tags={$tag}'>{$tag}</a></li>";
        }
        $content .= "</ul>";
        return new Response($content);
    }

    /**
     * @param array $news
     * @return Response
     */
    public function renderNews(array $news)
    {
        $content = '';
        foreach ($news as $article) {
            $content .= $this->doRenderArticleText($article);
        }
        return new Response($content);
    }

    /**
     * @param array $article
     * @return string
     */
    public function renderArticle(array $article)
    {
        return new Response($this->doRenderArticleText($article));
    }

    /**
     * @param $help
     * @return string
     */
    public function renderHelp($help)
    {
        $help = str_replace("```\n", '', htmlentities($help));
        return new Response("<pre>$help</pre>");
    }

    /**
     * @param ErrorResponse $response
     * @return Response
     */
    private function renderError(ErrorResponse $response)
    {
        $content = "<h1>Error</h1>" . $response->content;
        return new Response($content, $response->code);
    }

    /**
     * @param array $article
     * @return string
     */
    private function doRenderArticleText(array $article)
    {
        $text = nl2br(htmlentities($article['text']));
        return <<<ENDS
            <h1><a href='/news/{$article['id']}'>{$article['title']}</a></h1>
            <p>Source: <a href="{$article['sourceurl']}">{$article['sourceurl']}</a></p>
            <p>Id: {$article['id']}</p>
            <p>Published: {$article['published']}</p>
            <p>Tags: {$article['tags']}</p>
            <p>$text</p>
ENDS;
    }
}