<?php
/**
 * kipelovets <kipelovets@mail.ru>
 */

namespace News\View;

use News\Controller\ErrorResponse;
use Symfony\Component\HttpFoundation\JsonResponse;

class Json implements ViewInterface
{
    /**
     * @param $route
     * @param $data
     * @return JsonResponse
     */
    public function render($route, $data)
    {
        if ($data instanceof ErrorResponse) {
            return new JsonResponse([
                'error' => [
                    'message' => $data->content,
                ],
            ], $data->code);
        }

        $resp = new JsonResponse(['response' => $data]);
        $resp->setEncodingOptions(JSON_UNESCAPED_UNICODE);
        return $resp;
    }
}