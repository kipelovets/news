<?php
/**
 * kipelovets <kipelovets@mail.ru>
 */

namespace News\View;

class ViewFactory 
{
    /**
     * @param $acceptHeader
     * @return Html|Json
     */
    public static function create($acceptHeader)
    {
        if ($acceptHeader === 'application/json') {
            return new Json();
        }
        return new Html();
    }
}