<?php
/**
 * kipelovets <kipelovets@mail.ru>
 */

namespace News\View;

use Symfony\Component\HttpFoundation\Response;

interface ViewInterface
{
    /**
     * @param $route
     * @param $data
     * @return Response
     */
    public function render($route, $data);
}