<?php
/**
 * kipelovets <kipelovets@mail.ru>
 */

namespace News\View;

use Pimple\Container;
use Pimple\ServiceProviderInterface;
use Silex\Api\EventListenerProviderInterface;
use Silex\Application;
use Symfony\Component\EventDispatcher\EventDispatcherInterface;
use Symfony\Component\HttpKernel\Event\GetResponseEvent;
use Symfony\Component\HttpKernel\Event\GetResponseForControllerResultEvent;
use Symfony\Component\HttpKernel\KernelEvents;

class ViewProvider implements ServiceProviderInterface, EventListenerProviderInterface
{
    /** @var ViewInterface $view */
    private $view;

    /** @var string $routeName*/
    private $routeName;

    /**
     * Registers services on the given container.
     *
     * This method should only be used to configure services and parameters.
     * It should not get services.
     *
     * @param Container $pimple An Container instance
     */
    public function register(Container $pimple)
    {
    }

    /**
     * @param Container $app
     * @param EventDispatcherInterface $dispatcher
     */
    public function subscribe(Container $app, EventDispatcherInterface $dispatcher)
    {
        $dispatcher->addListener(KernelEvents::REQUEST, function (GetResponseEvent $event) {
            $req = $event->getRequest();
            $this->routeName = $req->get('_route');
            $this->view = ViewFactory::create($req->headers->get('Accept', 'text/html'));
        });

        $dispatcher->addListener(KernelEvents::VIEW, function (GetResponseForControllerResultEvent $event) {
            if (!$this->view) {
                $this->view = ViewFactory::create(null);
            }
            $res = $event->getControllerResult();
            $event->setResponse($this->view->render($this->routeName, $res));
        });
    }
}